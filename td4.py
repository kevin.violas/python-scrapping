# Exercice 1

import requests
import re
UNI = requests.get("http://www.univ-orleans.fr")
print(UNI.text)
print(UNI.encoding)

 # Exercice 2

from bs4 import BeautifulSoup
soup = BeautifulSoup(UNI.text, 'lxml')

print(soup.find_all('div'))
print(soup.find_all('img'))
print(soup.find_all('a'))

print(soup.find_all('div','composite-zone'))

# Exercice 3

STACK = requests.get("https://stackoverflow.com/questions/tagged/beautifulsoup")
soup = BeautifulSoup(STACK.text, 'lxml')

# Question 1

questions = []
votes = []
answers = []

for node in soup.findAll('a', 'question-hyperlink'):
    questions.append(node.findAll(text=True)) # récupère toutes les questions

for node in soup.findAll('span', 'vote-count-post'):
    votes.append(node.findAll(text=True)) # récupère tous les votes

for node in soup.findAll('div', 'status'):
    answers.append(node.findAll(text=True)) # récupère le nombre de réponses

questions_10 = []
votes_10 = []
answers_10 = []

for i in range(0,10): # affiche les dix premières questions et leurs votes ainsi que leurs réponses
    #print(questions[i])
    questions_10.append(questions[i])
    #print(votes[i])
    votes_10.append(votes[i])
    #print(answers[i])
    answers_10.append(answers[i])

questions_10[2] = str(questions_10[2]).replace(","," ")

# Question 3
import collections

import pandas
df = pandas.DataFrame({
    "questions": questions_10,
    "votes": votes_10,
    "answers": answers_10
})

df.to_csv("data.csv", index=0)


# Exercice 4

FORM = requests.get("http://formation.univ-orleans.fr/fr/formation/rechercher-une-formation.html#nav")
soup = BeautifulSoup(FORM.text, 'lxml')

#print(soup.find('.results-placeholder'))


# Exercice 5

# Question 1
def getDefinition(x):
    URL = "http://services.aonaware.com/DictService/Default.aspx?action=define&dict=*&query={0}".format(x)
    print(URL)
    html = requests.get(URL)
    soup = BeautifulSoup(html.text, 'lxml')
    res = []
    for node in soup.findAll('pre'):
        #print(node.findAll(text=True)) # définitions
        res.append(node.findAll(text=True))
    return res

getDefinition("lol")

# Question 2

lines = []
with open("vocabulary.txt") as f:
    lines = f.readlines()
    f = open('definitions.txt', 'w') # remets le fichier à zéro
    f.close()
    fic = open("definitions.txt", "a")
    fic.write("")
    for line in lines:
        test = getDefinition(line)
        fic.write(str(test)+"\n")
